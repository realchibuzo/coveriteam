#!/bin/bash

# SPDX-FileCopyrightText: 2021 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

# TODO still 1 issue pending:
# steps for packing:
#   - clone coveriteam
#     - copy coveriteam, bin, lib
#     - copy yml and cvt file, also resource yml file
#     - prepare the tools directory: copy cst_transform, download tools and unzip
#   - maybe have a list of verifiers somewhere.
#     This would be used for: labelling as well as downloading tools

# TODO how to we deal with licenses of the zipped tools?

set -eao pipefail
# IFS=$'\t\n'

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
DIR_COVERITEAM="coveriteam"
# Don't want to put cst_tranform in verifiers as it is not a verifier.
VERIFIERS=$(< "$SCRIPT_DIR/verifiers")

# Create temp directory and push it
TMPDIR=$(mktemp -d)
# TODO update this variable. This should be the name of the tool, as in the benchdef.
TOZIP="portfolio"
pushd "$TMPDIR" > /dev/null
mkdir $TOZIP

# Clone coveriteam
git clone $SCRIPT_DIR/../../../coveriteam
pushd $DIR_COVERITEAM

# TODO remove this after merging in master.
git checkout portfolio

# Download and install the actors:
CACHE_DIR="cache"
for verifier in $VERIFIERS;
  do
    ./bin/coveriteam --cache-dir $CACHE_DIR "actors/$verifier.yml" --tool-info
done

# for the cst_transform
# ./bin/coveriteam --cache-dir $CACHE_DIR "actors/feature-extractor.yml" --tool-info

# Delete the zip archives. It reduces the size of the archive.
rm $CACHE_DIR/archives/*
# Need to create some file, as vcloud does not trasnfer an empty dir. 
touch $CACHE_DIR/archives/dummy

# CoVeriTeam part is finished at this point.
popd

# Start adding the files to the directory to be zipped.

# Copy coveriteam, bin, lib, and licenses.
cp -r $DIR_COVERITEAM/{bin,coveriteam,lib,LICENSE,LICENSES} "$TOZIP/"
# Add the cvt file.
# TODO It doesn't need to be copied in the examples folder.
# TODO Maybe we can put the cvt file in the contrib along with this script? 
mkdir "$TOZIP/examples/"
cp $DIR_COVERITEAM/examples/portfolio.cvt "$TOZIP/examples/"

# Copy required actor definitions yml. Also, resource yml.
# TODO maybe it is a better idea to have these files in this along with this script.
# Then it is not dependent on the CoVeriTeam repo.
mkdir "$TOZIP/actors"
for verifier in $VERIFIERS;
  do
    cp "$DIR_COVERITEAM/actors/$verifier.yml" "$TOZIP/actors/"
    # Also copy the classifier yml for the too.
    # cp "$DIR_COVERITEAM/actors/classifier-$verifier.yml" "$TOZIP/actors/"
done

# for the cst_transform
# cp "$DIR_COVERITEAM/actors/feature-extractor.yml" "$TOZIP/actors/"
# cp "$DIR_COVERITEAM/actors/cst-transform.yml" "$TOZIP/actors/"

# Individually copy the remaining YML files.
cp "$SCRIPT_DIR/verifier_resource.yml" "$TOZIP/actors/"
cp "$SCRIPT_DIR/portfolio-verifier-reach-safety-single-run.xml" "$TOZIP/"
cp "$SCRIPT_DIR/portfolio-verifier-reach-safety-test-suite.xml" "$TOZIP/"

# Copy the cache directory
mv $DIR_COVERITEAM/cache $TOZIP/


zip -r "$TOZIP.zip" "$TOZIP"
popd
mv "$TMPDIR/$TOZIP.zip" ./
rm -rf $TMPDIR
