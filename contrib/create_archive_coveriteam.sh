#!/bin/bash

# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

set -eao pipefail
IFS=$'\t\n'

if ! git diff --quiet; then
  echo "Archive directory is dirty. Please commit pending changes and then re-run this script."
  false
fi


DIRNAME="$(dirname "$(readlink -f "$0")")/.."
TMPDIR=$(mktemp -d)
pushd "$TMPDIR" > /dev/null
ln -s "$DIRNAME" coveriteam
zip --exclude="*/lib/PyYAML-3.13.dist-info/*" --exclude="*/lib/yaml*" --exclude="*/test/*" --exclude="*/a.out" --exclude="*/.idea/*" --exclude="*/__pycache__/*" -r coveriteam.zip coveriteam/{bin,coveriteam,doc,lib,examples,LICENSE,LICENSES,README.md,actors,smoke_test_all_tools.sh,CHANGELOG.md}
popd
mv "$TMPDIR/coveriteam.zip" ./
echo "Wrote coveriteam.zip"
