<!--
This file is part of CoVeriTeam,
a tool for on-demand composition of cooperative verification systems:
https://gitlab.com/sosy-lab/software/coveriteam

SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->


## Python code execution
It is possible to modify and execute CoVeriTeams generated python code directly. 
By passing the flag `--gen-code` when running CoVeriTeam, it will print the generated code. 
By adding some new python lines, the code can be executed:
```python
#!/usr/bin/env python3
import glob
from pathlib import Path
import os
import sys

project_dir = Path("/path/to/coveriteam/directory").resolve()
lib_dir = project_dir / "lib"
for wheel in glob.glob(os.path.join(lib_dir, "*.whl")):
    sys.path.insert(0, wheel)

sys.path.insert(0, str(project_dir))
sys.path.append(str(lib_dir))

sys.setrecursionlimit(5000)

import coveriteam.util as util
util.set_cache_directories() # Add a different cache directory as parameter if needed 
util.set_cache_update(True) # True, if you want to download or updated the tools, otherwise False


# The generated code
```
The file can be executed with `./filename.py` given you marked it as executable (`chmod +x filename.py`).

## Full example
A full example with the `verifier.cvt` executed from the `examples` directory:
```python
#!/usr/bin/env python3
import glob
from pathlib import Path
import os
import sys

project_dir = Path("../").resolve()
lib_dir = project_dir / "lib"
for wheel in glob.glob(os.path.join(lib_dir, "*.whl")):
    sys.path.insert(0, wheel)

sys.path.insert(0, str(project_dir))
sys.path.append(str(lib_dir))

sys.setrecursionlimit(5000)

import coveriteam.util as util
util.set_cache_directories()
util.set_cache_update(True)

# The generated code
data_model = 'ILP32'
specification_path = 'test-data/properties/unreach-call.prp'
program_path = 'test-data/c/error.i'
verifier_path = '../actors/cpa-seq.yml'
verifier_version = 'default'
from coveriteam.language.actor import Actor
Actor.trust_tool_info = False
Actor.allow_cgroup_access = False

from coveriteam.actors.testers import *
from coveriteam.actors.misc import *
from coveriteam.actors.analyzers import *
from coveriteam.language.artifact import *
from coveriteam.language.composition import *
from coveriteam.language.utilactors import *
from coveriteam.language.parallel_portfolio import ParallelPortfolio


verifier = ProgramVerifier(verifier_path, verifier_version)
print(verifier)
program = Program.create(program_path,data_model)
specification = BehaviorSpecification(specification_path)
inputs = {'program': program,'spec': specification,}
result = verifier.act_and_save_xml(**inputs)

print(result)
```
