// This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
// https://gitlab.com/sosy-lab/software/coveriteam
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

// Prepare inputs.
witness = ArtifactFactory.create(Witness, witness_path);
program = ArtifactFactory.create(CProgram, program_path, data_model);
spec = ArtifactFactory.create(BehaviorSpecification, specification_path);
input_for_algo_selection = {'program':program, 'spec':spec};
input_for_metaval = {'program':program, 'spec':spec, 'witness':witness};

// Select an appropriate verifier backend.
verifier_selector = ActorFactory.create(AlgorithmSelector, "../../actors/algo-selector-metaval.yml", "default");
selected_verifier = execute(verifier_selector, input_for_algo_selection);

// First stage: instrumentation of witness and program.
ins = ActorFactory.create(WitnessInstrumentor, "../../actors/cpa-witnesses-instrumentor.yml", "witness-instrumentor");

// Second stage: use the verifier backend.
verifier_def = selected_verifier.actordef;
verifier = ActorFactory.create(ProgramVerifier, verifier_def);
metaval = SEQUENCE(ins, verifier);

// Print type information about the composition (for illustration)
print(metaval);

// Execute metaval on the inputs.
res = execute(metaval, input_for_metaval);
print("The following artifacts were produced by the execution:");
print(res);
